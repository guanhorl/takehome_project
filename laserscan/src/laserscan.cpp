#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/Float32.h>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

int NUM_READING = 100;
float FREQUENCY = 40.0;
float ANGLE_MIN = -1.57;    //[rad]
float ANGLE_MAX = 1.57; //[rad]
float RANGE_MIN = 0.0;  //[m]
float RANGE_MAX = 5.0;    //[m]

unsigned int seq;
float ANGLE_INCRE;
float TIME_INCRE;

void setParameter(ros::NodeHandle& nh){
  
  nh.getParam("/laserscan/NUM_READING", NUM_READING);
  nh.getParam("/laserscan/FREQUENCY", FREQUENCY);
  nh.getParam("/laserscan/ANGLE_MAX", ANGLE_MAX);
  nh.getParam("/laserscan/ANGLE_MIN", ANGLE_MIN);
  nh.getParam("/laserscan/RANGE_MIN", RANGE_MIN);
  nh.getParam("/laserscan/RANGE_MAX", RANGE_MAX);
  
  ANGLE_INCRE = (ANGLE_MAX-ANGLE_MIN) / NUM_READING;
  TIME_INCRE = (1/FREQUENCY) / NUM_READING;
}

std::vector<float> getRanges(){
  std::srand(std::time(0));
  std::vector<float> ranges;
  for (unsigned int i=0; i < NUM_READING; i++){
    float var = (float)std::rand() / (float)RAND_MAX;
    ranges.push_back(1.0 + var/50);
  }
  return ranges;
}


int main(int argc, char** argv){

  // Initialize the ROS system
  ros::init(argc, argv, "laserscan");
  ros::NodeHandle nh;
  ros::Publisher lsPub = nh.advertise<sensor_msgs::LaserScan>("laser", 50);
  ros::Rate loop_rate(10);

  // set global variable with rosparam
  setParameter(nh);
  ROS_INFO("set parameter complete");

  while(ros::ok()) {
    // create fake laserscan msg
    sensor_msgs::LaserScan lsmsg;
    lsmsg.header.stamp = ros::Time::now();
    lsmsg.header.seq = seq++;
    lsmsg.header.frame_id = std::string("laser_frame"); 

    lsmsg.angle_min = ANGLE_MIN;
    lsmsg.angle_max = ANGLE_MAX;
    lsmsg.angle_increment = ANGLE_INCRE;

    lsmsg.time_increment = TIME_INCRE;
    lsmsg.scan_time = 0.0;
    
    lsmsg.range_min = RANGE_MIN;
    lsmsg.range_max = RANGE_MAX;

    lsmsg.ranges = getRanges();
    std::vector<float> intensities(NUM_READING,0.0);
    lsmsg.intensities = intensities;
    
    // publish msg
    lsPub.publish(lsmsg);
    ROS_INFO("publish LaserScan complete. seq: %4d",seq);
    
    ros::spinOnce();
    loop_rate.sleep();
  }

}

